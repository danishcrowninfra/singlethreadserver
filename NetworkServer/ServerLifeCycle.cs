﻿using NetworkObjectFramework;
using NetworkObjectFramework.NetworkServerImplementation;

namespace NetworkServer
{
    public class ServerLifeCycle : IServerLifeCycle
    {
        public bool IsRunning { get; set; }
    }
}