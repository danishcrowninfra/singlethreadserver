﻿using System;
using System.Runtime.Serialization.Formatters.Binary;
using Common;
using Loggers;
using NetworkObjectFramework;
using NetworkObjectFramework.GeneralNetwork.NetworkDeserializer;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketDecoder;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketEncoder;
using NetworkObjectFramework.GeneralNetwork.NetworkSerializer;
using NetworkObjectFramework.NetworkServerImplementation;
using NetworkObjectFramework.NetworkServerImplementation.SocketRelated;
using NetworkObjectFramework.Tools;

namespace NetworkServer
{
    class Program
    {
        private static readonly ILogger Logger = new DelegateLogger((s, ll) => Console.WriteLine(s));
        static void Main(string[] args)
        {
            var formatter = new BinaryFormatter();
            var server = new NetworkObjectFramework.NetworkServerImplementation.NetworkServer(Logger, new SocketHelperMethods());
            var listningSocketWrapper = new SocketWrapper(
                socket: SocketAdapter.GetListner(8000, 10),
                logger: Logger,
                networkDeserializer: new NetworkDeserializer(
                    formatter: formatter,
                    packetDecoder: new NetworkPacketDecoder()),
                networkSerializer: new NetworkSerializer(
                    logger: Logger,
                    packetEncoder: new NetworkPacketEncoder(formatter)), 
                isListner: true);
            server.Listen(listningSocketWrapper);
            server.ObjectReceived += ObjectReceived;
            Logger.Log("Starting server");
            server.Run(new ServerLifeCycle() { IsRunning = true });
        }

        private static void ObjectReceived(object sender, NetworkServerEventArgs e)
        {
            var receivedObject = e.ReceivedObject as Person;
            Logger.Log($"{receivedObject.Navn}({receivedObject.Cpr} from {e.SocketWrapper.Socket.RemoteEndPoint})");
            e.SocketWrapper.Send(new Person() {Cpr = "999999-9999", Navn = "Niern fra Ålborg'a"});
        }
    }
}
