﻿using System;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using NetworkObjectFramework;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketEncoder;
using static NetworkObjectFramework.Tools.ClientTools;

namespace NetworkClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var command = "";
            while (command != "q")
            {
                Console.WriteLine("Press enter to connect");
                Console.ReadLine();
                var client = new TcpClient();
                try
                {
                    client.Connect("127.0.0.1", 8000);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.GetType().Name} {e.Message}");
                }
                while (client.Connected)
                {
                    Console.WriteLine("Press enter to send one person\r\nnumber to send multible persons and q to quit");
                    command = Console.ReadLine();
                    if (command == "q") break;
                    if (command == "r")
                    {
                        try
                        {
                            var bufer = new byte[1024];
                            var receivedBytes = client.Client.Receive(bufer);
                            Console.WriteLine($"Received {receivedBytes} bytes");
                            continue;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"{e.GetType().Name} {e.Message}");
                            break;
                        }
                    }
                    if (command == "ra")
                    {
                        try
                        {
                            var bufer = new byte[1024];
                            int receivedBytes;
                            do
                            {
                                receivedBytes = client.Client.Receive(bufer);
                                Console.WriteLine($"Received {receivedBytes} bytes");
                            } while (receivedBytes > 0);
                            continue;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"{e.GetType().Name} {e.Message}");
                            break;
                        }
                    }
                    int count;
                    if (!int.TryParse(command, out count)) count = 1;
                    try
                    {
                        foreach (var person in GetRandomPersons(count))
                        {
                            NetworkPacketEncoder networkPacketEncoder = new NetworkPacketEncoder(new BinaryFormatter());
                            client.Client.Send(networkPacketEncoder.GetSerializedObjectWithHeader(person));
                        }
                        Console.WriteLine($"Send {count} person(s)");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"{e.GetType().Name} {e.Message}");
                    }
                }
            }
        }
    }
}
