using System;
using System.Collections;
using System.Collections.Generic;
using Common;
using NetworkObjectFramework;
using NetworkObjectFramework.NetworkServerImplementation;
using NetworkObjectFramework.NetworkServerImplementation.SocketRelated;
using NetworkObjectFramework.Tools;
using NSubstitute;
using NUnit.Framework;

namespace NetworkObjectFrameworkTests
{
    [TestFixture]
    public class NetworkServerTests
    {
        private ILogger _logger;
        private ISocketHelperMethods _socketHelperMethods;
        private ISocketWrapper _listningSocketWrapper;
        private NetworkServer _networkServer;

        [SetUp]
        public void SetUp()
        {
            _logger = Substitute.For<ILogger>();
            _socketHelperMethods = Substitute.For<ISocketHelperMethods>();
            _networkServer = new NetworkServer(_logger, _socketHelperMethods);
            _listningSocketWrapper = Substitute.For<ISocketWrapper>();
            _listningSocketWrapper.IsListner.Returns(true);
            _networkServer.Listen(_listningSocketWrapper);
        }

        [Test]
        public void Run_without_call_to_listen_throws_NetworkServerException()
        {
            _networkServer = new NetworkServer(_logger, _socketHelperMethods);
            var lifeCycle = ConstructLifeCycleThatReturns(0);

            Assert.Throws<NetworkServerException>(() => { _networkServer.Run(lifeCycle); });
        }

        [Test]
        public void Run_returns_when_lifeCycle_IsRunning_Changes_to_false()
        {
            var lifeCycle = ConstructLifeCycleThatReturns(1);

            _networkServer.Run(lifeCycle);

            _socketHelperMethods.Received(1).SocketSelect(Arg.Any<IList<ISocketAdapter>>(), Arg.Any<IList<ISocketAdapter>>(), Arg.Any<IList<ISocketAdapter>>(), Arg.Any<int>());
        }

        [Test]
        public void Run_Calls_Accept_and_raises_event_if_Listner_Has_ReadData()
        {
            //Not very transparent, but this is actually the setup for one run where one listner has data, so run should call accept
            var lifeCycle = ConstructLifeCycleThatReturns(1);
            var clientConnectFired = false;

            _networkServer.ClientConnect += (sender, args) => clientConnectFired = true;

            _networkServer.Run(lifeCycle);

            _listningSocketWrapper.Received().Accept();
            Assert.IsTrue(clientConnectFired);
        }

        [Test]
        public void Run_calls_read_on_socketWrapper_and_fires_ObjectReceived_two_times()
        {
            var readSocketAdapter = Substitute.For<ISocketWrapper>();
            readSocketAdapter.Socket.Returns(Substitute.For<ISocketAdapter>());
            readSocketAdapter.Read().Returns(info => new[] {new Person(), new Person()});
            _listningSocketWrapper.Accept().Returns(readSocketAdapter);
            _networkServer = new NetworkServer(_logger, _socketHelperMethods);
            _networkServer.Listen(_listningSocketWrapper);
            var lifeCycle = ConstructLifeCycleThatReturns(2);
            var timesObjectReceivedFired = 0;
            _networkServer.ObjectReceived += (sender, args) => timesObjectReceivedFired++;

            _networkServer.Run(lifeCycle);

            readSocketAdapter.Received().Read();
            Assert.AreEqual(2, timesObjectReceivedFired);
        }

        private static IServerLifeCycle ConstructLifeCycleThatReturns(int numberOfRuns)
        {
            var serverLifeCycle = Substitute.For<IServerLifeCycle>();
            var i = 0;
            serverLifeCycle.IsRunning.Returns(x => i++ < numberOfRuns);
            return serverLifeCycle;
        }
        private class SocketHelperMethodsMock : ISocketHelperMethods
        {
            private readonly Action<IList<ISocketAdapter>> _socketSelectReadAction;
            private readonly Action<IList<ISocketAdapter>> _socketSelectWriteAction;
            private readonly Action<IList<ISocketAdapter>> _socketSelectErrorAction;
            public SocketHelperMethodsMock(Action<IList<ISocketAdapter>> socketSelectReadAction = null, Action<IList<ISocketAdapter>> socketSelectWriteAction = null, Action<IList<ISocketAdapter>> socketSelectErrorAction = null)
            {
                _socketSelectReadAction = socketSelectReadAction ?? delegate(IList<ISocketAdapter> list) { };
                _socketSelectWriteAction = socketSelectWriteAction ?? delegate (IList<ISocketAdapter> list) { };
                _socketSelectErrorAction = socketSelectErrorAction ?? delegate (IList<ISocketAdapter> list) { };
            }

            public void SocketSelect(IList<ISocketAdapter> read, IList<ISocketAdapter> write, IList<ISocketAdapter> error, int micro)
            {
                _socketSelectReadAction(read);
                _socketSelectWriteAction(write);
                _socketSelectErrorAction(error);
            }

        }
    }
}