﻿using System;
using System.Linq;
using Common;
using NetworkObjectFramework.GeneralNetwork.NetworkDeserializer;
using NetworkObjectFramework.GeneralNetwork.NetworkSerializer;
using NetworkObjectFramework.NetworkServerImplementation;
using NetworkObjectFramework.NetworkServerImplementation.SocketRelated;
using NetworkObjectFramework.Tools;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net.Sockets;

namespace NetworkObjectFrameworkTests
{
    [TestFixture]
    public class SocketWrapperTests
    {
        public INetworkSerializer Serializer;
        public INetworkDeserializer Deserializer;
        public ISocketAdapter Socket;
        public SocketWrapper SocketWrapper;
        [SetUp]
        public void SetUp()
        {
            Socket = Substitute.For<ISocketAdapter>();
            var logger = Substitute.For<ILogger>();
            Deserializer = Substitute.For<INetworkDeserializer>();
            Serializer = Substitute.For<INetworkSerializer>();
            SocketWrapper = new SocketWrapper(Socket, logger, Deserializer, Serializer);
        }

        [Test]
        public void HasDataToSend_Is_False()
        {
            Assert.IsFalse(SocketWrapper.HasDataToSend);
        }

        [Test]
        public void Send_HasDataToSend_Is_True_If_object_send()
        {
            Serializer.Serialize(Arg.Any<object>()).Returns(DataHelpers.GetBytes(10));

            SocketWrapper.Send(null);

            Assert.IsTrue(SocketWrapper.HasDataToSend);
        }

        [Test]
        public void Read_Returns_Empty_IEnum()
        {
            var result = SocketWrapper.Read();

            CollectionAssert.IsEmpty(result);
        }

        [Test]
        public void If_You_Read_And_Socket_Returns_0_Bytes_Then_The_Socket_Is_CLosed()
        {
            foreach (var obj in SocketWrapper.Read()) {}
            
            Assert.IsTrue(SocketWrapper.Closed);
        }

        [Test]
        public void WhenSocketReturnsDataDeserializerIsCalledWithTheCorrectData()
        {
            var data = DataHelpers.GetBytes(100);
            Socket.Receive(Arg.Any<byte[]>()).Returns(info =>
            {
                var tmp = info.Arg<byte[]>();
                Buffer.BlockCopy(data, 0, tmp, 0, data.Length);
                return data.Length;
            });
            var usedBuffer = new byte[0];
            Deserializer.DataReceived(Arg.Any<FifoBuffer>()).Returns(info =>
            {
                usedBuffer = info.Arg<FifoBuffer>().InternalBuffer;
                return Enumerable.Empty<Person>();
            });

            foreach (var obj in SocketWrapper.Read()) { }

            Assert.AreEqual(data, usedBuffer);
        }
        [Test]
        public void WhenDeserializerReturnsTwoObjectsReadReturnsTheSameObjects()
        {
            var listOfObjects = new List<Person>()
                {
                    new Person() {Navn = "Hans"},
                    new Person() {Navn = "Niels"}
                };
            Socket.Receive(Arg.Any<byte[]>()).Returns(info => info.Arg<byte[]>().Length);
            Deserializer.DataReceived(Arg.Any<FifoBuffer>()).Returns(info => listOfObjects);

            var returnedObjects = SocketWrapper.Read();

            CollectionAssert.AreEqual(listOfObjects, returnedObjects);
        }

        [Test]
        public void WhenSocketReceiveThrowsAnExceptionSocketIsMarkedAsClosed()
        {
            Socket.Receive(Arg.Any<byte[]>()).Returns(info =>
            {
                throw new SocketException();
            });
            foreach (var obj in SocketWrapper.Read()) { }

            Assert.IsTrue(SocketWrapper.Closed);
        }

        [Test]
        public void AcceptReturnsSocketAdapterAccept()
        {
            var socketAdapter = Substitute.For<ISocketAdapter>();
            Socket.Accept().Returns(socketAdapter);

            var returnedSocketWrapper = SocketWrapper.Accept();

            Assert.AreSame(socketAdapter, returnedSocketWrapper.Socket);
        }
    }
}
