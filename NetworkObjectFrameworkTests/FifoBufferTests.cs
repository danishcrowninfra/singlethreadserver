﻿using System;
using NetworkObjectFramework.Tools;
using NUnit.Framework;
using static NetworkObjectFrameworkTests.DataHelpers;

namespace NetworkObjectFrameworkTests
{
    [TestFixture]
    public class FifoBufferTests
    {
        public FifoBuffer FifoBuffer;
        [SetUp]
        public void Setup()
        {
            FifoBuffer = new FifoBuffer();
        }

        [Test]
        public void InternalBuffer_Is_Not_Null()
        {
            Assert.NotNull(FifoBuffer.InternalBuffer);
        }

        [Test]
        public void Length_Is_0()
        {
            Assert.AreEqual(0, FifoBuffer.Length);
        }
        [Test]
        public void Legth_Is_10_if_10_pushed()
        {
            FifoBuffer.Push(GetBytes(10));

            Assert.AreEqual(10, FifoBuffer.Length);
        }
        [Test]
        public void Legth_Is_1024_if_1024_pushed()
        {
            FifoBuffer.Push(GetBytes(2048), 1024);

            Assert.AreEqual(1024, FifoBuffer.Length);
        }

        [Test]
        public void If_data_pushed_buffer_is_data()
        {
            var data = GetBytes(10);
            FifoBuffer.Push(data);

            Assert.AreEqual(data, FifoBuffer.InternalBuffer);
        }

        [Test]
        public void If_data_pushed_buffer_is_the_combination_of_that()
        {
            var data1 = GetBytes(10, 100);
            var data2 = GetBytes(10, 200);
            var expectedBuffer = CombineData(data1, data2);

            FifoBuffer.Push(data1);
            FifoBuffer.Push(data2);

            Assert.AreEqual(expectedBuffer, FifoBuffer.InternalBuffer);
        }

        [Test]
        public void If_data_pushed_pop_returns_that_data()
        {
            var data = GetBytes(10);
            FifoBuffer.Push(data);

            var returnedData = FifoBuffer.Pop(data.Length);

            Assert.AreEqual(data, returnedData);
        }

        [Test]
        public void If_data_pushed_pop_returns_combination_of_that()
        {
            var data1 = GetBytes(10, 100);
            var data2 = GetBytes(10, 200);
            var expectedData = CombineData(data1, data2);

            FifoBuffer.Push(data1);
            FifoBuffer.Push(data2);

            var returnedData = FifoBuffer.Pop(data1.Length + data2.Length);

            Assert.AreEqual(expectedData, returnedData);
        }

        [Test]
        public void Pop_can_return_part_of_pushed_data()
        {
            var data1 = DataHelpers.GetBytes(10, 100);
            var data2 = DataHelpers.GetBytes(10, 200);
            var expectedData = new byte[15];
            Buffer.BlockCopy(DataHelpers.CombineData(data1, data2), 0, expectedData, 0, 15);
            FifoBuffer.Push(data1);
            FifoBuffer.Push(data2);

            var returnedData = FifoBuffer.Pop(15);

            Assert.AreEqual(expectedData, returnedData);
        }

        [Test]
        public void Its_ok_to_push_empty_array()
        {
            var data = new byte[0];

            FifoBuffer.Push(data);

            Assert.AreEqual(data, FifoBuffer.InternalBuffer);
        }

        [Test]
        public void push_null_throws_ArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => FifoBuffer.Push(null));
        }

        [Test]
        public void pop_more_than_in_buffer_throws_FifoBufferException()
        {
            Assert.Throws<FifoBufferException>(() => FifoBuffer.Pop(10));
        }

        [Test]
        public void pop_negative_value_throws_FifoBufferException()
        {
            Assert.Throws<FifoBufferException>(() => FifoBuffer.Pop(-10));
        }

        [Test]
        public void pop_zero_throws_FifoBufferException()
        {
            Assert.Throws<FifoBufferException>(() => FifoBuffer.Pop(0));
        }
    }
}
