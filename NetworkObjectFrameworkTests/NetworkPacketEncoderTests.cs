using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using NetworkObjectFramework;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketEncoder;
using NetworkObjectFramework.Tools;
using NUnit.Framework;

namespace NetworkObjectFrameworkTests
{
    [TestFixture]
    public class NetworkPacketEncoderTests
    {
        [TestCase(0)]
        [TestCase(100)]
        public void GetSerializedObjectWithHeader_returns_correct_encoded_packet_of_size(int size)
        {
            var serializedObject = DataHelpers.GetBytes(size);
            var bytesOfLength = BitConverter.GetBytes(serializedObject.Length);
            var completePacket = bytesOfLength.Concat(serializedObject);
            var formatter = new FormatterMock(serializedObject);
            var networkPacketEncoder = new NetworkPacketEncoder(formatter);

            var result = networkPacketEncoder.GetSerializedObjectWithHeader(new Person());

            Assert.AreEqual(completePacket, result);
        }

        private class FormatterMock : IFormatter
        {
            private readonly byte[] _serializedObject;

            public FormatterMock(byte[] serializedObject)
            {
                _serializedObject = serializedObject;
            }

            public object Deserialize(Stream serializationStream)
            {
                throw new NotImplementedException();
            }

            public void Serialize(Stream serializationStream, object graph)
            {
                serializationStream.Write(_serializedObject, 0, _serializedObject.Length);
            }

            public ISurrogateSelector SurrogateSelector { get; set; }
            public SerializationBinder Binder { get; set; }
            public StreamingContext Context { get; set; }
        }
    }
}