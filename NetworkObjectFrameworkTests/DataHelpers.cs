using System;

namespace NetworkObjectFrameworkTests
{
    public static class DataHelpers
    {
        public static byte[] CombineData(byte[] data1, byte[] data2)
        {
            var tmp = new byte[data1.Length + data2.Length];
            Buffer.BlockCopy(data1, 0, tmp, 0, data1.Length);
            Buffer.BlockCopy(data2, 0, tmp, data1.Length, data2.Length);
            return tmp;
        }

        public static byte[] GetBytes(int count, byte startNumber)
        {
            var b = startNumber;
            var bytes = new byte[count];
            for (var i = 0; i < count; i++)
                bytes[i] = b++;
            return bytes;
        }
        public static byte[] GetBytes(int count)
        {
            return GetBytes(count, 0);
        }
    }
}