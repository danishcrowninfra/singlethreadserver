using System.Runtime.InteropServices;
using Common;
using NetworkObjectFramework;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketEncoder;
using NetworkObjectFramework.GeneralNetwork.NetworkSerializer;
using NetworkObjectFramework.Tools;
using NSubstitute;
using NUnit.Framework;

namespace NetworkObjectFrameworkTests
{
    [TestFixture]
    public class NetworkSerializerTests
    {
        [Test]
        public void bla()
        {
            var logger = Substitute.For<ILogger>();
            var packetEncoder = Substitute.For<IPacketEncoder>();
            var networkSerializer = new NetworkSerializer(logger, packetEncoder);
            var serializedObjectWithHeader = DataHelpers.GetBytes(10);
            packetEncoder.GetSerializedObjectWithHeader(Arg.Any<object>()).Returns(serializedObjectWithHeader);

            var result = networkSerializer.Serialize(new Person());

            Assert.AreEqual(serializedObjectWithHeader, result);
        }
    }
}