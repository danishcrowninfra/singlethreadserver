using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using NetworkObjectFramework;
using NetworkObjectFramework.GeneralNetwork.NetworkDeserializer;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketDecoder;
using NetworkObjectFramework.Tools;
using NSubstitute;
using NUnit.Framework;

namespace NetworkObjectFrameworkTests
{
    [TestFixture]
    public class NetworkDeserializerTests
    {
        public FifoBuffer FifoBuffer;
        public IFormatter Formatter;
        public IPacketDecoder PacketDecoder;
        public NetworkDeserializer NetworkDeserializer;
        [SetUp]
        public void SetUp()
        {
            FifoBuffer = new FifoBuffer();
            Formatter = Substitute.For<IFormatter>();
            PacketDecoder = Substitute.For<IPacketDecoder>();
            NetworkDeserializer = new NetworkDeserializer(Formatter, PacketDecoder);
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(10)]
        [TestCase(100)]
        public void DataReceived_Returns_multible_results(int numberOfObjectsToReturn)
        {
            SetUp_NetworkDeserializer_so_it_returns_number_of_objects(numberOfObjectsToReturn);

            var result = new List<object>(NetworkDeserializer.DataReceived(new FifoBuffer()));

            Assert.AreEqual(numberOfObjectsToReturn, result.Count);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(Person));
        }

        private void SetUp_NetworkDeserializer_so_it_returns_number_of_objects(int numberOfObjectsToReturn)
        {
            var numberOfObjectsReturned = 0;
            var data = new byte[0];
            PacketDecoder.GetNextPacket(Arg.Any<FifoBuffer>()).Returns(x => numberOfObjectsReturned++ < numberOfObjectsToReturn);
            PacketDecoder.Packet.Returns(new DecodedNetworkPacket() { Data = data });
            Formatter.Deserialize(Arg.Any<Stream>()).Returns(new Person());
        }
    }
}