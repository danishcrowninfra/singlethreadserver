﻿using System;
using System.Collections.Generic;
using NetworkObjectFramework;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketDecoder;
using NetworkObjectFramework.Tools;
using NSubstitute;
using NUnit.Framework;
using static NetworkObjectFrameworkTests.DataHelpers;

namespace NetworkObjectFrameworkTests
{
    [TestFixture]
    class NetworkPacketDecoderTests
    {
        public FifoBuffer FifoBuffer;
        public NetworkPacketDecoder NetworkPacketDecoder;
        [SetUp]
        public void SetUp()
        {
            FifoBuffer = new FifoBuffer();
            NetworkPacketDecoder = new NetworkPacketDecoder();
        }

        [Test]
        public void GetNextPacket_returns_false()
        {
            Assert.IsFalse(NetworkPacketDecoder.GetNextPacket(FifoBuffer));
        }

        [Test]
        public void GetNextPacket_returns_true_when_dummy_packet_pushed()
        {
            PushDummyPacket(FifoBuffer, 100);
            Assert.IsTrue(NetworkPacketDecoder.GetNextPacket(FifoBuffer));
        }

        [Test]
        public void GetNextPacket_returns_decoded_data_pushed()
        {
            var data = GetBytes(100);
            PushDummyPacket(FifoBuffer, data);

            NetworkPacketDecoder.GetNextPacket(FifoBuffer);

            Assert.AreEqual(data, NetworkPacketDecoder.Packet.Data);
        }

        [Test]
        public void GetNextPacket_two_poped_packets_returns_two_decoded_packets()
        {
            var data1 = GetBytes(100);
            var data2 = GetBytes(100,100);
            PushDummyPacket(FifoBuffer, data1);
            PushDummyPacket(FifoBuffer, data2);
            var decodedNetworkPackets = new DecodedNetworkPacket[2];

            var i = 0;
            while (NetworkPacketDecoder.GetNextPacket(FifoBuffer))
                decodedNetworkPackets[i++] = NetworkPacketDecoder.Packet;

            Assert.AreEqual(data1, decodedNetworkPackets[0].Data);
            Assert.AreEqual(data2, decodedNetworkPackets[1].Data);
        }

        [Test]
        public void GetNextPacket_one_and_half_pushed_packet_returns_one_packet()
        {
            var data1 = GetBytes(100);
            var data2 = GetBytes(100, 100);
            PushDummyPacket(FifoBuffer, data1);
            PushPartOfPacket(FifoBuffer, GetDummyPacket(data2), 20);
            var listOfDecodedPackets = new List<DecodedNetworkPacket>();

            while (NetworkPacketDecoder.GetNextPacket(FifoBuffer))
                listOfDecodedPackets.Add(NetworkPacketDecoder.Packet);

            Assert.AreEqual(1, listOfDecodedPackets.Count);
        }

        [Test]
        public void GetNextPacket_too_short_header_returns_false()
        {
            FifoBuffer.Push(GetBytes(2));

            Assert.IsFalse(NetworkPacketDecoder.GetNextPacket(FifoBuffer));
        }

        [Test]
        public void GetNextPacket_4_byte_header()
        {
            FifoBuffer.Push(GetBytes(4));

            Assert.IsFalse(NetworkPacketDecoder.GetNextPacket(FifoBuffer));
        }

        private static void PushPartOfPacket(FifoBuffer fifoBuffer, FifoBuffer packet, int size)
        {
            fifoBuffer.Push(packet.Pop(size));
        }

        private static void PushDummyPacket(FifoBuffer fifoBuffer, int sizeOfData)
        {
            fifoBuffer.Push(GetDummyPacket(GetBytes(sizeOfData)).InternalBuffer);
        }
        private static void PushDummyPacket(FifoBuffer fifoBuffer, byte[] data)
        {
            fifoBuffer.Push(GetDummyPacket(data).InternalBuffer);
        }

        private FifoBuffer GetDummyPacket(int sizeOfData)
        {
            var tmp = new FifoBuffer();
            tmp.Push(BitConverter.GetBytes(sizeOfData));
            tmp.Push(GetBytes(sizeOfData));
            return tmp;
        }

        private static FifoBuffer GetDummyPacket(byte[] data)
        {
            var tmp = new FifoBuffer();
            tmp.Push(BitConverter.GetBytes(data.Length));
            tmp.Push(data);
            return tmp;
        }

    }
}
