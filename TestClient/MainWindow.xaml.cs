﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Windows;
using Common;
using Loggers;
using NetworkObjectFramework;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketEncoder;
using NetworkObjectFramework.GeneralNetwork.NetworkSerializer;
using NetworkObjectFramework.NetworkServerImplementation;
using NetworkObjectFramework.NetworkServerImplementation.SocketRelated;
using NetworkObjectFramework.Tools;

namespace TestClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private TcpClient _client;
        private readonly ILogger _logger;
        public MainWindow()
        {
            InitializeComponent();
            _client = new TcpClient();
            _logger = new DelegateLogger((s, level) => LogBox.Text += $"{level} - {s}{Environment.NewLine}");
        }

        private async void SendPerson_Click(object sender, RoutedEventArgs e)
        {
            int numberOfPersons;
            try
            {
                numberOfPersons = int.Parse(TBcount.Text);
            }
            catch (FormatException)
            {
                TBcount.Text = "1";
                return;
            }
            var buffer = new FifoBuffer();
            var enumerator = ClientTools.GetRandomPersons(numberOfPersons).GetEnumerator();
            var networkSerializer = new NetworkSerializer(_logger, new NetworkPacketEncoder(new BinaryFormatter()));
            while (enumerator.MoveNext())
            {
                buffer.Push(networkSerializer.Serialize(enumerator.Current));
                if (buffer.Length < 1500) continue;
                try
                {
                    await SendData(buffer);
                }
                catch (SocketException ex)
                {
                    _logger.Log(ex.Message);
                    DisconnectClient();
                    return;
                }
            }
            if (buffer.Length > 0) await SendData(buffer);
        }

        private async Task SendData(FifoBuffer buffer)
        {
            _logger.Log("Send data");
            await Task.Run(() => _client.Client.Send(buffer.Pop(buffer.Length > 1500 ? 1500 : buffer.Length)));
        }

        private async void connectBtn_Click(object sender, RoutedEventArgs e)
        {
            IPAddress[] ipAdd;
            int port;
            try
            {
                ipAdd = await Dns.GetHostAddressesAsync(TBhost.Text);
                port = int.Parse(TBport.Text);
            }
            catch (Exception ex)
                when (ex is SocketException || ex is ArgumentOutOfRangeException || ex is ArgumentException)
            {
                _logger.Log($"Error: {ex.Message}");
                TBhost.Text = "127.0.0.1";
                return;
            }
            catch (Exception ex) when (ex is FormatException || ex is OverflowException)
            {
                _logger.Log($"Error: {ex.Message}");
                TBport.Text = "8000";
                return;
            }
            try
            {
                await _client.ConnectAsync(ipAdd, port);
            }
            catch (SocketException ex)
            {
                _logger.Log($"Error: {ex.Message}");
                return;
            }
            _logger.Log($"Connected to {_client.Client.RemoteEndPoint}");
            connectBtn.IsEnabled = false;
            disconnectBtn.IsEnabled = true;
            SendPerson.IsEnabled = true;
        }

        private void disconnectBtn_Click(object sender, RoutedEventArgs e)
        {
            DisconnectClient();
        }

        private void DisconnectClient()
        {
            _client.Close();
            _client = new TcpClient();
            SendPerson.IsEnabled = false;
            disconnectBtn.IsEnabled = false;
            connectBtn.IsEnabled = true;
            _logger.Log("Client disconnected");
        }
    }
}
