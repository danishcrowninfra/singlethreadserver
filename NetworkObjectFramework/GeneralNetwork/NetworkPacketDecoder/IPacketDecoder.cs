using NetworkObjectFramework.Tools;

namespace NetworkObjectFramework.GeneralNetwork.NetworkPacketDecoder
{
    public interface IPacketDecoder
    {
        bool GetNextPacket(FifoBuffer fifoBuffer);

        DecodedNetworkPacket Packet { get; }
    }
}