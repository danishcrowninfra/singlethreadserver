using System;
using NetworkObjectFramework.Tools;

namespace NetworkObjectFramework.GeneralNetwork.NetworkPacketDecoder
{
    public class NetworkPacketDecoder : IPacketDecoder
    {
        private int _dataSize;
        private bool _isReceivingHeader = true;

        public bool GetNextPacket(FifoBuffer fifoBuffer)
        {
            if (_isReceivingHeader)
            {
                if (fifoBuffer.Length < 4) return false;
                _dataSize = GetDataSize(fifoBuffer);
                _isReceivingHeader = false;
            }
            if (fifoBuffer.Length < _dataSize) return false;
            Packet = new DecodedNetworkPacket() { Data = fifoBuffer.Pop(_dataSize), DataSize = _dataSize };
            _isReceivingHeader = true;
            return true;
        }
        public DecodedNetworkPacket Packet { get; private set; }
        private int GetDataSize(FifoBuffer fifoBuffer)
        {
            var tmp = fifoBuffer.Pop(sizeof(int));
            return BitConverter.ToInt32(tmp, 0);
        }
    }
}