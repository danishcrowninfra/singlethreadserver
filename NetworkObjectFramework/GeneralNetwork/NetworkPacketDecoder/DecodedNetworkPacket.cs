namespace NetworkObjectFramework.GeneralNetwork.NetworkPacketDecoder
{
    public class DecodedNetworkPacket
    {
        public int DataSize { get; set; }
        public byte[] Data { get; set; }
    }
}