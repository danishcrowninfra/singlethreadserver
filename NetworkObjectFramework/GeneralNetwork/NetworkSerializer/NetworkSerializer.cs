using Common;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketEncoder;

namespace NetworkObjectFramework.GeneralNetwork.NetworkSerializer
{
    public class NetworkSerializer : INetworkSerializer
    {
        private readonly ILogger _logger;
        private readonly IPacketEncoder _packetEncoder;

        public NetworkSerializer(ILogger logger, IPacketEncoder packetEncoder)
        {
            _logger = logger;
            _packetEncoder = packetEncoder;
        }

        public byte[] Serialize(object objectToSerialize)
        {
            _logger.Log("Serializing an object and queing it to be send");
            return _packetEncoder.GetSerializedObjectWithHeader(objectToSerialize);
        }
    }
}