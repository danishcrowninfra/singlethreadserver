namespace NetworkObjectFramework.GeneralNetwork.NetworkSerializer
{
    public interface INetworkSerializer
    {
        byte[] Serialize(object obj);
    }
}