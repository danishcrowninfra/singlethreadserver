using System;
using System.Runtime.Serialization;

namespace NetworkObjectFramework.GeneralNetwork.NetworkDeserializer
{
    [Serializable]
    public class NetworkDeserializerException : Exception
    {
        public NetworkDeserializerException()
        {
        }

        public NetworkDeserializerException(string message) : base(message)
        {
        }

        public NetworkDeserializerException(string message, Exception inner) : base(message, inner)
        {
        }

        protected NetworkDeserializerException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}