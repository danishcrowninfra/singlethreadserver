using System.Collections.Generic;
using NetworkObjectFramework.Tools;

namespace NetworkObjectFramework.GeneralNetwork.NetworkDeserializer
{
    public interface INetworkDeserializer
    {
        IEnumerable<object> DataReceived(FifoBuffer buffer);
    }
}