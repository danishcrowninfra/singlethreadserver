using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using NetworkObjectFramework.GeneralNetwork.NetworkPacketDecoder;
using NetworkObjectFramework.Tools;

namespace NetworkObjectFramework.GeneralNetwork.NetworkDeserializer
{
    public class NetworkDeserializer : INetworkDeserializer
    {
        private readonly IFormatter _formatter;
        private readonly IPacketDecoder _packetDecoder;

        public NetworkDeserializer(IFormatter formatter, IPacketDecoder packetDecoder)
        {
            _formatter = formatter;
            _packetDecoder = packetDecoder;
        }

        public IEnumerable<object> DataReceived(FifoBuffer buffer)
        {
            while (_packetDecoder.GetNextPacket(buffer))
                yield return DeserializeObject(_packetDecoder.Packet.Data);
        }

        private object DeserializeObject(byte[] data)
        {
            var ms = new MemoryStream(data);
            var deserializedObject = _formatter.Deserialize(ms);
            return deserializedObject;
        }
    }
}