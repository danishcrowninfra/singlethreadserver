using System;
using System.IO;
using System.Runtime.Serialization;

namespace NetworkObjectFramework.GeneralNetwork.NetworkPacketEncoder
{
    public class NetworkPacketEncoder : IPacketEncoder
    {
        private readonly IFormatter _formatter;

        public NetworkPacketEncoder(IFormatter formatter)
        {
            _formatter = formatter;
        }

        public byte[] GetSerializedObjectWithHeader(object objectToSerialize)
        {
            const int sizeOfInt = sizeof(int);
            var serializedObject = GetSerializedObject(objectToSerialize);
            var serializedObjectSize = serializedObject.Length;
            var totalSize = serializedObjectSize + sizeOfInt;
            var buffer = new byte[totalSize];
            Buffer.BlockCopy(IntToBytes(serializedObjectSize), 0, buffer, 0, sizeOfInt);
            Buffer.BlockCopy(serializedObject, 0, buffer, sizeOfInt, serializedObjectSize);
            return buffer;
        }
        private byte[] GetSerializedObject(object obj)
        {
            var ms = new MemoryStream();
            _formatter.Serialize(ms, obj);
            return ms.ToArray();
        }
        private static byte[] IntToBytes(int i)
        {
            return BitConverter.GetBytes(i);
        }
    }
}