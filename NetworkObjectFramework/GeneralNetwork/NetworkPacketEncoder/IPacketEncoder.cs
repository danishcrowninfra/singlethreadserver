namespace NetworkObjectFramework.GeneralNetwork.NetworkPacketEncoder
{
    public interface IPacketEncoder
    {
        byte[] GetSerializedObjectWithHeader(object objectToSerialize);
    }
}