using System.Collections.Generic;

namespace NetworkObjectFramework.Tools
{
    public static class LinqExtensionSplit
    {
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> list, int sizeOfParts)
        {
            var enumerator = list.GetEnumerator();
            var tmp = new List<T>();
            while (enumerator.MoveNext())
            {
                tmp.Add(enumerator.Current);
                if (tmp.Count != sizeOfParts) continue;
                yield return tmp;
                tmp.Clear();
            }
            if (tmp.Count > 0) yield return tmp;
        }
    }
}