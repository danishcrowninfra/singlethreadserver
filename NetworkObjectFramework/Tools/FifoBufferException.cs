using System;

namespace NetworkObjectFramework.Tools
{
    public class FifoBufferException : Exception
    {
        public FifoBufferException(string message) : base(message)
        {
        }
    }
}