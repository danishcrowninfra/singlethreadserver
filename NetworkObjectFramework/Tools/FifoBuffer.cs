using System;

namespace NetworkObjectFramework.Tools
{
    public class FifoBuffer
    {
        public byte[] InternalBuffer{ get; private set; } = new byte[0];
        public int Length => InternalBuffer.Length;
        public void Push(byte[] data, int count)
        {
            var newBuffer = new byte[InternalBuffer.Length + count];
            Buffer.BlockCopy(InternalBuffer, 0, newBuffer, 0, InternalBuffer.Length);
            Buffer.BlockCopy(data, 0, newBuffer, InternalBuffer.Length, count);
            InternalBuffer = newBuffer;
        }

        public void Push(byte[] data)
        {
            if (data == null) throw new ArgumentNullException();
            Push(data, data.Length);
        }

        public byte[] Pop(int count)
        {
            if (count > InternalBuffer.Length) throw new FifoBufferException("Tryied to pop more than in buffer");
            if (count < 0) throw new FifoBufferException("Tryied to pop negative value");
            if (count == 0) throw new FifoBufferException("Tryied to pop zero bytes");
            var bytesPulled = new byte[count];
            Buffer.BlockCopy(InternalBuffer, 0, bytesPulled, 0, count);
            var newBuffer = new byte[InternalBuffer.Length - count];
            Buffer.BlockCopy(InternalBuffer, count, newBuffer, 0, InternalBuffer.Length - count);
            InternalBuffer = newBuffer;
            return bytesPulled;
        }
    }
}