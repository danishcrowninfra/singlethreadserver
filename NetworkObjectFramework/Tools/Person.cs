﻿using System;

namespace NetworkObjectFramework.Tools
{
    [Serializable]
    public class Person
    {
        public virtual string GetTitel()
        {
            return "Person";
        }
        public string Navn { get; set; }
        public string Cpr { get; set; }
    }
}
