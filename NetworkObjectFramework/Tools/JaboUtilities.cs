﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace NetworkObjectFramework.Tools
{
    public static class JaboUtilities
    {
        public static int GetSizeOfNextObject(Stream stream)
        {
            if (stream.Length < 4) throw new Exception();
            var buffer = new byte[4];
            stream.Read(buffer, 0, 4);
            return BitConverter.ToInt32(buffer, 0);
        }

        public static byte[] GetSerializedObject(object obj)
        {
            var ms = new MemoryStream();
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(ms, obj);
            return ms.ToArray();
        }

        public static T GetNextObject<T>(Stream stream)
        {
            var sizeOfObject = GetSizeOfNextObject(stream);
            if (stream.Length < sizeOfObject) throw new Exception();
            var binaryFormater = new BinaryFormatter();
            return (T)binaryFormater.Deserialize(stream);
        }

        public static void SendObject(Stream stream, object obj)
        {
            const int sizeOfInt = 4;
            var serializedObject = GetSerializedObject(obj);
            var sizeOfBuffer = serializedObject.Length + sizeOfInt;
            var buffer = new byte[sizeOfBuffer];
            var sizeBytes = BitConverter.GetBytes(serializedObject.Length);
            Array.Copy(sizeBytes, buffer, sizeOfInt);
            Array.Copy(serializedObject, 0, buffer, sizeOfInt, serializedObject.Length);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static void SendSerializedObject(Stream stream, byte[] serializedObject)
        {
            stream.Write(serializedObject, 0, serializedObject.Length);
        }
    }
}