﻿using System;
using System.Collections.Generic;

namespace NetworkObjectFramework.Tools
{
    public class ClientTools
    {
        private static readonly Random Random = new Random();
        private static readonly string[] Fornavne = { "Abdullah", "Hussein", "Michael", "Daniel", "Mikkel", "Søren", "Simon", "Klaus", "Stig", "Arild", "Niels", "Børge", "Jørgen", "Henry" };
        private static readonly string[] Efternavne = { "Garde", "Blaasen", "Calmar", "Sommer", "Christensen", "Mark", "Nielsen", "Bendtsen" };

        private static string GetRandomNavn()
        {
            return $"{Fornavne[Random.Next(Fornavne.Length - 1)]} {Efternavne[Random.Next(Efternavne.Length - 1)]}";
        }

        private static string GetRandomCpr()
        {
            var s = "";
            for (var i = 0; i < 6; i++)
            {
                s += Random.Next(9);
            }
            s += "-";
            for (var i = 0; i < 4; i++)
            {
                s += Random.Next(9);
            }
            return s;
        }

        public static Person GetRandomPerson()
        {
            return new Person()
            {
                Cpr = GetRandomCpr(),
                Navn = GetRandomNavn(),
            };
        }

        public static IEnumerable<Person> GetRandomPersons(int count)
        {
            for (var i = 0; i < count; i++)
            {
                yield return GetRandomPerson();
            }
        }
    }
}
