using System;

namespace NetworkObjectFramework.NetworkServerImplementation
{
    public class NetworkServerException : Exception
    {
        public NetworkServerException(string message) : base(message)
        {
        }
        public NetworkServerException()
        {
        }
    }
}