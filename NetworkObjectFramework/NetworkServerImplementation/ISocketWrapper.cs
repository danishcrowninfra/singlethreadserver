using System;
using System.Collections;
using NetworkObjectFramework.NetworkServerImplementation.SocketRelated;

namespace NetworkObjectFramework.NetworkServerImplementation
{
    public interface ISocketWrapper : IDisposable
    {
        bool HasDataToSend { get; }
        bool IsListner { get; }
        ISocketAdapter Socket { get; }
        bool Closed { get; }
        IEnumerable Read();
        ISocketWrapper Accept();
        void Send(object obj);
        void ProcessWrite();
    }
}