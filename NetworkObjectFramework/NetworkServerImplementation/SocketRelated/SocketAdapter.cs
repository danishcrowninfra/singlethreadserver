using System;
using System.Net;
using System.Net.Sockets;

namespace NetworkObjectFramework.NetworkServerImplementation.SocketRelated
{
    public class SocketAdapter : ISocketAdapter
    {
        private readonly Socket _socket;

        public SocketAdapter(Socket socket)
        {
            if (socket == null) throw new ArgumentNullException(nameof(socket));
            _socket = socket;
        }

        public static ISocketAdapter GetListner(int port, int backLog)
        {
            var ipEndPoint = new IPEndPoint(IPAddress.Any, port);
            var listner = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listner.Bind(ipEndPoint);
            listner.Listen(backLog);
            return new SocketAdapter(listner);
        }

        public int Receive(byte[] buffer)
        {
            return _socket.Receive(buffer);
        }

        public ISocketAdapter Accept()
        {
            return new SocketAdapter(_socket.Accept());
        }

        public bool Blocking
        {
            get { return _socket.Blocking; }
            set { _socket.Blocking = value; }
        }

        public EndPoint RemoteEndPoint => _socket.RemoteEndPoint;

        public EndPoint LocalEndPoint => _socket.LocalEndPoint;

        public bool Connected => _socket.Connected;

        public int Send(byte[] buffer)
        {
            return _socket.Send(buffer);
        }

        public Socket GetSocket()
        {
            return _socket;
        }

        public override string ToString()
        {
            string remoteEndpoint;
            try
            {
                remoteEndpoint = _socket.RemoteEndPoint.ToString();
            }
            catch (SocketException ex) when (ex.ErrorCode == 10057)
            {
                remoteEndpoint = "No remote end point";
            }
            catch (ObjectDisposedException)
            {
                return "Disposed socket";
            }
            return $"{_socket.LocalEndPoint} - {remoteEndpoint}";
        }

        public void Dispose()
        {
            try
            {
                _socket.Close();
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}