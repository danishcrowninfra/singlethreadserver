using System;
using System.Net;

namespace NetworkObjectFramework.NetworkServerImplementation.SocketRelated
{
    public interface ISocketAdapter : IDisposable
    {
        int Receive(byte[] buffer);
        ISocketAdapter Accept();
        bool Blocking { get; set; }
        EndPoint RemoteEndPoint { get; }
        EndPoint LocalEndPoint { get; }
        bool Connected { get; }
        int Send(byte[] buffer);
    }
}