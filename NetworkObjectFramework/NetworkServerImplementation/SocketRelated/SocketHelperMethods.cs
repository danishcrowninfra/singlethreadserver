using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;

namespace NetworkObjectFramework.NetworkServerImplementation.SocketRelated
{
    public class SocketHelperMethods : ISocketHelperMethods
    {
        public void SocketSelect(
            IList<ISocketAdapter> checkRead, 
            IList<ISocketAdapter> checkWrite, 
            IList<ISocketAdapter> checkError,
            int micro)
        {
            var read = checkRead.Select(socketAdapter => ((SocketAdapter) socketAdapter).GetSocket()).ToList();
            var write = checkWrite.Select(socketAdapter => ((SocketAdapter) socketAdapter).GetSocket()).ToList();
            var error = checkError.Select(socketAdapter => ((SocketAdapter) socketAdapter).GetSocket()).ToList();

            Socket.Select(read, write, error, micro);

            foreach (var sa in checkRead.ToArray())
            {
                var socketAdapter = (SocketAdapter) sa;
                if (!read.Contains(socketAdapter.GetSocket())) checkRead.Remove(sa);
            }
            foreach (var sa in checkWrite.ToArray())
            {
                var socketAdapter = (SocketAdapter)sa;
                if (!write.Contains(socketAdapter.GetSocket())) checkWrite.Remove(sa);
            }
            foreach (var sa in checkError.ToArray())
            {
                var socketAdapter = (SocketAdapter)sa;
                if (!error.Contains(socketAdapter.GetSocket())) checkError.Remove(sa);
            }
        }
    }
}