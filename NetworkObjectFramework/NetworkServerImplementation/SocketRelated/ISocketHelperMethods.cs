using System.Collections.Generic;

namespace NetworkObjectFramework.NetworkServerImplementation.SocketRelated
{
    public interface ISocketHelperMethods
    {
        void SocketSelect(IList<ISocketAdapter> read, IList<ISocketAdapter> write, IList<ISocketAdapter> error, int micro);
    }
}