namespace NetworkObjectFramework.NetworkServerImplementation
{
    public interface IServerLifeCycle
    {
        bool IsRunning { get; set; }
    }
}