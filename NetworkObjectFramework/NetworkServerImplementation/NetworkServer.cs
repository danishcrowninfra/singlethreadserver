using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using Common;
using NetworkObjectFramework.NetworkServerImplementation.SocketRelated;

namespace NetworkObjectFramework.NetworkServerImplementation
{
    public class NetworkServer
    {
        private readonly ILogger _logger;
        private readonly ISocketHelperMethods _socketHelperMethods;

        public NetworkServer(ILogger logger, ISocketHelperMethods socketHelperMethods)
        {
            _logger = logger;
            _socketHelperMethods = socketHelperMethods;
            Sockets = new List<ISocketWrapper>();
            _logger.Log($"New {nameof(NetworkServer)} created.");
        }
        public List<ISocketWrapper> Sockets { get; set; }

        public void Listen(ISocketWrapper socketWrapper)
        {
            Sockets.Add(socketWrapper);
            _logger.Log($"Listning on {socketWrapper.Socket.LocalEndPoint}");
        }

        public void Run(IServerLifeCycle lifeCycle)
        {
            if (Sockets.Count == 0) throw new NetworkServerException("You have to call Listen() before Run()");
            _logger.Log("Server running");
            while (lifeCycle.IsRunning)
            {
                var read = new List<ISocketAdapter>(Sockets.Select(sw => sw.Socket));
                var write = new List<ISocketAdapter>(Sockets.Where(sw => sw.HasDataToSend).Select(sw => sw.Socket));
                var error = new List<ISocketAdapter>(Enumerable.Empty<ISocketAdapter>());
                try
                {
                    _socketHelperMethods.SocketSelect(read, write, error, 1000);
                }
                catch (SocketException ex)
                {
                    _logger.Log($"Socket.Select threw SocketException you should never see this message {ex.GetType().Name} {ex.Message}");
                }
                HandleRead(read);
                HandleWrite(write);
                CleanUpClosedSockets();
            }
        }

        private void CleanUpClosedSockets()
        {
            foreach (var socketWrapper in Sockets.ToArray().Where(socketWrapper => socketWrapper.Closed))
            {
                RemoveSocketWrapper(socketWrapper);
            }
        }

        private void HandleRead(IEnumerable<ISocketAdapter> readList)
        {
            foreach (var socket in readList)
            {
                _logger.Log("Something to read");
                var socketWrapper = Sockets.First(sw => sw.Socket == socket);
                if (socketWrapper.IsListner) AddSocketWrapper(socketWrapper.Accept());
                else ReadData(socketWrapper);
            }
        }
        private void HandleWrite(IEnumerable<ISocketAdapter> writeList)
        {
            var socketWrappersToWrite = writeList.Select(sa => Sockets.First(sw => sw.Socket == sa));
            foreach (var socketWrapper in socketWrappersToWrite)
            {
                _logger.Log("Something to write");
                socketWrapper.ProcessWrite();
            }
        }

        private void AddSocketWrapper(ISocketWrapper socketWrapper)
        {
            _logger.Log($"Client connected {socketWrapper.Socket.RemoteEndPoint}");
            Sockets.Add(socketWrapper);
            OnClientConnect(socketWrapper);
        }
        private void RemoveSocketWrapper(ISocketWrapper socketWrapper)
        {
            _logger.Log($"Client disconnected {socketWrapper.Socket.RemoteEndPoint}");
            Sockets.Remove(socketWrapper);
            OnClientDisconnect(socketWrapper);
            socketWrapper.Dispose();
        }

        private void ReadData(ISocketWrapper socketWrapper)
        {
            _logger.Log($"Reading Data from {socketWrapper.Socket.RemoteEndPoint}");
            foreach (var obj in socketWrapper.Read())
                OnObjectReceived(socketWrapper, obj);
        }

        public event EventHandler<NetworkServerEventArgs> ObjectReceived;
        private void OnObjectReceived(ISocketWrapper socketWrapper, object receivedObject)
        {
            ObjectReceived?.Invoke(this, new NetworkServerEventArgs(socketWrapper, receivedObject));
        }
        public event EventHandler<NetworkServerEventArgs> ClientConnect;
        private void OnClientConnect(ISocketWrapper socketWrapper)
        {
            ClientConnect?.Invoke(this, new NetworkServerEventArgs(socketWrapper));
        }
        public event EventHandler<NetworkServerEventArgs> ClientDisconnect;
        private void OnClientDisconnect(ISocketWrapper socketWrapper)
        {
            ClientDisconnect?.Invoke(this, new NetworkServerEventArgs(socketWrapper));
        }
    }
}