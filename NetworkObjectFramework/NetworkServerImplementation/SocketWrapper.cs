using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using Common;
using NetworkObjectFramework.GeneralNetwork.NetworkDeserializer;
using NetworkObjectFramework.GeneralNetwork.NetworkSerializer;
using NetworkObjectFramework.NetworkServerImplementation.SocketRelated;
using NetworkObjectFramework.Tools;

namespace NetworkObjectFramework.NetworkServerImplementation
{
    public class SocketWrapper : ISocketWrapper
    {
        public SocketWrapper(ISocketAdapter socket, ILogger logger, INetworkDeserializer networkDeserializer, INetworkSerializer networkSerializer, bool isListner = false)
        {
            _logger = logger;
            _networkDeserializer = networkDeserializer;
            _networkSerializer = networkSerializer;
            Socket = socket;
            _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Created");
            IsListner = isListner;
        }

        private readonly FifoBuffer _sendBuffer = new FifoBuffer();
        private readonly FifoBuffer _receiveBuffer = new FifoBuffer();
        public bool HasDataToSend => _sendBuffer.Length > 0;
        public void Send(object obj)
        {
            _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Pushing data to send buffer");
            _sendBuffer.Push(_networkSerializer.Serialize(obj));
        }

        public bool IsListner { get; }

        public ISocketAdapter Socket { get; }

        private readonly INetworkDeserializer _networkDeserializer;
        private readonly INetworkSerializer _networkSerializer;
        public bool Closed { get; private set; }
        private readonly ILogger _logger;

        public IEnumerable Read()
        {
            var buffer = new byte[2048];
            int receivedBytes;
            try
            {
                receivedBytes = Socket.Receive(buffer);
            }
            catch (SocketException e)
            {
                _logger.Log($"{nameof(SocketWrapper)} for {Socket}: {e.GetType().Name} {e.Message}");
                Closed = true;
                yield break;
            }

            if (receivedBytes == 0)
            {
                _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Received 0 bytes, socket is closed");
                Closed = true;
                yield break;
            }
            _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Received {receivedBytes} pushing data to receive buffer");
            _receiveBuffer.Push(buffer, receivedBytes);
            foreach (var obj in _networkDeserializer.DataReceived(_receiveBuffer))
            {
                _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Returning deserialized object");
                yield return obj;
            }
        }

        public ISocketWrapper Accept()
        {
            var socket = Socket.Accept();
            _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Accept() returning {nameof(SocketWrapper)} for {socket}");
            socket.Blocking = false;
            return new SocketWrapper(socket, _logger, _networkDeserializer, _networkSerializer);
        }

        public void ProcessWrite()
        {
            _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Sending {_sendBuffer.Length} bytes");
            if (_sendBuffer.Length == 0)
            {
                _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Got ProcessWrite with 0 bytes in buffer??");
                return;
            }
            try
            {
                var bytesSend = Socket.Send(_sendBuffer.InternalBuffer);
                _sendBuffer.Pop(bytesSend);
                _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Send {bytesSend} bytes still missing {_sendBuffer.Length} bytes");
            }
            catch (SocketException e) when (e.NativeErrorCode == 10035)
            {
                _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Would block");
            }
        }

        public void Dispose()
        {
            _logger.Log($"{nameof(SocketWrapper)} for {Socket}: Disposed");
            Socket.Dispose();
        }
    }
}
