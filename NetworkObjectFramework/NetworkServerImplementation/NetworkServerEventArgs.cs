using System;

namespace NetworkObjectFramework.NetworkServerImplementation
{
    public class NetworkServerEventArgs : EventArgs
    {
        public NetworkServerEventArgs(ISocketWrapper socketWrapper, object receivedObject)
        {
            SocketWrapper = socketWrapper;
            ReceivedObject = receivedObject;
        }
        public NetworkServerEventArgs(ISocketWrapper socketWrapper)
        {
            SocketWrapper = socketWrapper;
        }
        public ISocketWrapper SocketWrapper { get; private set; }
        public object ReceivedObject { get; private set; }
    }
}